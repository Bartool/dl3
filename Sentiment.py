"""
.. module:: SentimentTwAir

SentimentTwAir
*************

:Description: SentimentTwAir


:Version: 

:Created on: 07/09/2017 9:05 

"""

import pandas
from sklearn.metrics import confusion_matrix, classification_report
import re
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Activation, Embedding
from keras.layers import LSTM, GRU, BatchNormalization
from keras.optimizers import RMSprop, SGD
from keras.utils import np_utils
from keras.callbacks import EarlyStopping
from collections import Counter
import argparse
import time
import math


def calculate_statistics(review_len):
    sum_of_numbers = sum(number * review_len[number] for number in review_len)
    count = sum(review_len[number] for number in review_len)

    average = sum_of_numbers / count
    print('AVERAGE: ', average)

    total_squares = sum(number * number * review_len[number] for number in review_len)
    mean_of_squares = total_squares / count
    variance = mean_of_squares - average * average
    std_dev = math.sqrt(variance)
    print("DEVIATION: ", std_dev)

    review_len_elements = review_len.elements()
    median = sorted(review_len_elements)[int((count + 1) / 2)]
    print("MEDIAN: ", median)


def review_to_words(raw_review):
    """
    Only keeps ascii characters in the review and discards @words

    :param raw_review:
    :return:
    """

    try:
        letters_only = re.sub("[^a-zA-Z@]", " ", raw_review)
    except:
        return ""
    words = letters_only.lower().split()
    meaningful_words = [w for w in words if not re.match("^[@]", w)]
    return " ".join(meaningful_words)


def draw_plots(history):
    ##Store Plot
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    # Accuracy plot
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    # No validation loss in this example
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig('model_accuracy.pdf')
    plt.close()
    # Loss plot
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig('model_loss.pdf')


def prepare_data(Reviews, rows_to_load):
    number_ratings = [0, 0, 0, 0, 0]
    records_to_load = rows_to_load/10

    printed = False

    for i, row in Reviews.iterrows():
        if i % (rows_to_load/100) == 0:
            print("Data prep ", i/rows_to_load)

        rating = row['overall']
        try:
            text_split = row['reviewText'].split()
        except:
            Reviews = Reviews.drop(i)
            continue

        if len(text_split) > 70:
            Reviews = Reviews.drop(i)
            continue

        if number_ratings[rating - 1] < records_to_load / len(number_ratings):
            number_ratings[rating - 1] += 1
        else:
            Reviews = Reviews.drop(i)

        if sum(number_ratings) == records_to_load and not printed:
            print('Needed to load, ', i)
            printed = True

    Reviews = Reviews.sample(frac=1).reset_index(drop=True)  # shuffle

    Reviews.to_csv("minimized.csv")

    return Reviews


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', help="Verbose output (enables Keras verbose output)", action='store_true', default=False)
    parser.add_argument('--gpu', help="Use LSTM/GRU gpu implementation", action='store_true', default=False)
    args = parser.parse_args()

    verbose = 1 if args.verbose else 0
    impl = 2 if args.gpu else 0

    print("Starting:", time.ctime())

    ####################################
    # Data

    rows_to_load = 400000
    Reviews = pandas.read_csv("kindle_reviews.csv", nrows=rows_to_load)
    Reviews = prepare_data(Reviews, rows_to_load)

    # Reviews = pandas.read_csv("minimized.csv")

    # Pre-process the review and store in a separate column
    Reviews['clean_review'] = Reviews['reviewText'].apply(lambda x: review_to_words(x))
    # Convert sentiment to binary
    Reviews['sentiment'] = Reviews['overall'].apply(lambda x: x - 1)

    # Join all the words in review to build a corpus
    all_text = ' '.join(Reviews['clean_review'])
    words = all_text.split()

    # Convert words to integers
    counts = Counter(words)

    numwords = 5000  # Limit the number of words to use
    vocab = sorted(counts, key=counts.get, reverse=True)[:numwords]
    vocab_to_int = {word: ii for ii, word in enumerate(vocab, 1)}

    review_ints = []
    for each in Reviews['clean_review']:
        review_ints.append([vocab_to_int[word] for word in each.split() if word in vocab_to_int])

    # Create a list of labels
    labels = np.array(Reviews['sentiment'])

    # Find the number of reviews with zero length after the data pre-processing
    review_len = Counter([len(x) for x in review_ints])
    print("Zero-length reviews: {}".format(review_len[0]))
    print("Maximum review length: {}".format(max(review_len)))

    calculate_statistics(review_len)

    # Remove those reviews with zero length and its corresponding label
    review_idx = [idx for idx, review in enumerate(review_ints) if len(review) > 0]
    labels = labels[review_idx]
    Reviews = Reviews.iloc[review_idx]
    review_ints = [review for review in review_ints if len(review) > 0]

    seq_len = max(review_len)
    features = np.zeros((len(review_ints), seq_len), dtype=int)
    for i, row in enumerate(review_ints):
        features[i, -len(row):] = np.array(row)[:seq_len]

    split_frac = 0.8
    split_idx = int(len(features) * 0.8)
    train_x, val_x = features[:split_idx], features[split_idx:]
    train_y, val_y = labels[:split_idx], labels[split_idx:]

    test_idx = int(len(val_x) * 0.5)
    val_x, test_x = val_x[:test_idx], val_x[test_idx:]
    val_y, test_y = val_y[:test_idx], val_y[test_idx:]

    print("\t\t\tFeature Shapes:")
    print("Train set: \t\t{}".format(train_x.shape),
          "\nValidation set: \t{}".format(val_x.shape),
          "\nTest set: \t\t{}".format(test_x.shape))

    print("Train set: \t\t{}".format(train_y.shape),
          "\nValidation set: \t{}".format(val_y.shape),
          "\nTest set: \t\t{}".format(test_y.shape))

    ####################################
    # Model
    drop = 0.0
    nlayers = 2  # >= 1
    RNN = GRU  # LSTM

    neurons = 128
    embedding = 128

    model = Sequential()
    model.add(Embedding(numwords + 1, embedding, input_length=seq_len))

    if nlayers == 1:
        model.add(RNN(neurons, implementation=impl, recurrent_dropout=drop))
    else:
        model.add(RNN(neurons, implementation=impl, recurrent_dropout=drop, return_sequences=True))
        # model.add(BatchNormalization())
        for i in range(1, nlayers - 1):
            model.add(RNN(neurons, recurrent_dropout=drop, implementation=impl, return_sequences=True))
            # nn.add(BatchNormalization())
        model.add(RNN(neurons, recurrent_dropout=drop, implementation=impl))

    model.add(Dense(5))
    model.add(Activation('softmax'))

    ######################################
    # Training

    learning_rate = 0.01
    optimizer = SGD(lr=learning_rate, momentum=0.95)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

    epochs = 50
    batch_size = 100

    train_y_c = np_utils.to_categorical(train_y, 5)
    val_y_c = np_utils.to_categorical(val_y, 5)

    # Model visualization
    # We can plot the model by using the ```plot_model``` function. We need to install *pydot, graphviz and pydot-ng*
    # from keras.utils import plot_model
    # plot_model(model, to_file='model.png', show_shapes="true")
    earlystop = EarlyStopping(monitor='val_acc', min_delta=0.0001, patience=10, verbose=1, mode='auto')

    history = model.fit(train_x, train_y_c,
              batch_size=batch_size,
              epochs=epochs,
              validation_data=(val_x, val_y_c),
              verbose=verbose,
              callbacks=[earlystop])

    ############################################
    # Results

    test_y_c = np_utils.to_categorical(test_y, 5)
    score, acc = model.evaluate(test_x, test_y_c,
                                batch_size=batch_size,
                                verbose=verbose)
    print()
    print('Test ACC=', acc)

    test_pred = model.predict_classes(test_x, verbose=verbose)

    print()
    print('Confusion Matrix')
    print('-'*20)
    print(confusion_matrix(test_y, test_pred))
    print()
    print('Classification Report')
    print('-'*40)
    print(classification_report(test_y, test_pred))
    print()
    print("Ending:", time.ctime())

    draw_plots(history)
